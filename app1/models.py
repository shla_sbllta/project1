from django.db import models

# Create your models here.

class Login(models.Model):
    username = models.CharField(max_length=45)
    password = models.CharField(max_length=45)
    name = models.CharField(max_length=45)

    def __str__(self):
        return self.name