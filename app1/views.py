from django.shortcuts import render, redirect
from django.contrib import messages
from app1.models import Login
from django.contrib.auth.models import User, auth
from django.http import HttpResponse

# Create your views here.

def home(request):
    return render(request, "home.html")

def login(request):
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']

        user = auth.authenticate(username=username, password=password)
        if user is not None:
            auth.login(request, user)
            return redirect("home")
        else:
            messages.info(request, "Invalid credentials")
            return redirect("login")
    else:
        return render(request, 'login.html', {"title": "Login"})

def register(request):
    if request.method == "POST":
        first_name = request.POST['first_name']
        last_name = request.POST['last_name']
        username = request.POST['username']
        password = request.POST['password']

        # validation
        if User.objects.filter(username=username).exists():
            messages.info(request, "Username already exist")
            return redirect("register")
        else:
            User.objects.create_user(username=username, password=password, first_name=first_name, last_name=last_name).save()
            return redirect("home")
    else:
        return render(request, 'register.html', {"title": "Register"})


def logout(request):
    auth.logout(request)
    return redirect('home')
